---
title: Quilt
---

> Quilt is a powerful tool that allows you to capture source code changes
> without having a clean source tree.

[The yocto project
documentation](https://docs.yoctoproject.org/4.0.4/singleindex.html#using-quilt-in-your-workflow)


# Tutorial


The tutorial is to introduce you to quilt. It illustrates with examples
the various quilt commands and on how to manage patches. This tutorial
is released under the GNU Free Documentation License. The quilt project
is hosted at <http://savannah.nongnu.org/projects/quilt>. Have fun!

This tutorial, taken from
<http://www.shakthimaan.com/downloads.html#quilt-tutorial> is under the GNU Free
Documentation License.

Start by downloading this archive
<http://www.shakthimaan.com/downloads/glv/quilt-tutorial/quilt-tutorial.tar.bz2>.

# Tutorial

Create a directory and copy the given int.c, float.c and char.c files to
it. An ls should show:

```bash
$ ls
char.c float.c int.c
```

## new

Create a new patch:

```bash
$ quilt new int-1.diff
Patch int-1.diff is now on top
```

Quilt would have created the necessary folders:

```bash
$ ls -a
. .. char.c float.c int.c patches .pc
```

## top

The topmost patch of the stack can be displayed using the “top”
command:

```bash
$ quilt top
int-1.diff
```

## series

The patches available in the stack can be displayed using the “series”
command. The series file is present in the patches/ directory.

```bash
$ quilt series
int-1.diff
```

## add

Associate the file int.c to the topmost patch, which is int-1.diff.

```bash
$ quilt add int.c
File int.c added to patch int-1.diff
```

Now, all changes made to int.c will be updated in the int-1.diff
patch.

## edit

The edit command is used to edit a file with quilt:

```bash
$ quilt edit int.c
```
(Add a comment “/* integer */” to the end of the line “int x;”)

## refresh

The refresh command is used to update the patch:

```bash
File int.c is already in patch int-1.diff

$ quilt refresh
Refreshed patch int-1.diff
```

The patch is now updated. You can view the patch:

```bash
$ cat patches/int-1.diff
Index: quilt-eg/int.c
===================================
--- quilt-eg.orig/int.c 2006-04-05 22:00:50.000000000 +0530
+++ quilt-eg/int.c 2006-04-05 22:05:34.000000000 +0530
@@ -1,6 +1,6 @@
#include $<$stdio.h$>$
int main (void)
{
- int x;
+ int x; /* integer */
return 0;
}
```

## Repeating 1.1 - 1.6 for float.c

Create a new patch:

```bash
$ quilt new float-2.diff
Patch float-2.diff is now on top
```

The topmost patch in the stack is now:

```bash
$ quilt top
float-2.diff
```

The series now contains (the stack grows downwards):

```bash
$ quilt series
int-1.diff
float-2.diff
```

You can simply use quilt to edit a file, and it will automatically be
associated with the topmost patch.

```bash
$ quilt edit float.c
```

(Add “#include $<$math.h$>$” after “#include $<$stdio.h$>$”)

```bash
File float.c added to patch float-2.diff

$ quilt refresh
Refreshed patch float-2.diff
```

The patch is now updated. You can view the patch:

```bash
$ cat patches/float-2.diff
Index: quilt-eg/float.c
=====================================
--- quilt-eg.orig/float.c 2006-04-05 22:01:01.000000000 +0530
+++ quilt-eg/float.c 2006-04-05 22:11:11.000000000 +0530
@@ -1,4 +1,5 @@
#include $<$stdio.h$>$
+#include $<$math.h$>$
int main (void)
{
float f;
```

## Repeating 1.1 - 1.6 for char.c

Create a new patch:

```bash
$ quilt new char-3.diff
Patch char-3.diff is now on top
```

The topmost patch in the stack is now:

```bash
$ quilt top
char-3.diff
```

The series now contains:

```bash
$ quilt series
int-1.diff
float-2.diff
char-3.diff
```

You can simply use quilt to edit a file, and it will automatically be
associated with the topmost patch.

```bash
$ quilt edit char.c
```

(Add “#include $<$string.h” after “#include $<$stdio.h$>$”, and add a
comment “/* character */” to the end of the line “char ch;”)

```bash
File char.c added to patch char-3.diff

$ quilt refresh
Refreshed patch char-3.diff
```

The patch is now updated. You can view the patch:

```bash
$ cat patches/char-3.diff
Index: quilt-eg/char.c
=====================================
--- quilt-eg.orig/char.c 2006-04-05 22:01:17.000000000 +0530
+++ quilt-eg/char.c 2006-04-05 22:14:09.000000000 +0530
@@ -1,6 +1,7 @@
#include $<$stdio.h$>$
+#include $<$string.h$>$
int main (void)
{
- char ch;
+ char ch; /* character */
return 0;
}
```

## pop

The “pop” command is used to undo all patches upto (and NOT including)
the name of the patch provided.

```bash
$ quilt pop float-2
Removing patch char-3.diff
Restoring char.c

Now at patch float-2.diff
```

So, char.c has been restored:

```bash
$ cat char.c
#include $<$stdio.h$>$
int main (void)
{
char ch;
return 0;
}
```

The new top is now:

```bash
$ quilt top
float-2.diff
```

The series still contains the patches:

```bash
$ quilt series
int-1.diff
float-2.diff
char-3.diff
```

## applied

The list of applied patches can be viewed from the “applied” command:

```bash
$ quilt applied
int-1.diff
float-2.diff
```

## unapplied

The list of unapplied patches can be viewed from the “unapplied”
command:

```bash
$ quilt unapplied
char-3.diff
```

## previous

The patch below a patch can be displayed using the “previous” command:

```bash
$ quilt previous float-2
int-1.diff
```

## next

The patch above a patch can be displayed using the “next” command:

```bash
$ quilt next float-2
char-3.diff
```

## files

The list of files associated with the topmost patch can be displayed
using the “files” command:

```bash
$ quilt files
float.c
```

## patches

The list of patches associated with a given file can be displayed using
the “patches” command:

```bash
$ quilt patches int.c
int-1.diff
```

## push

The “push” command is used to apply patch(es) upto and including the
patch:

```bash
$ quilt push char-3
Applying patch char-3.diff
patching file char.c

Now at patch char-3.diff

$ quilt refresh
Refreshed patch char-3.diff

$ quilt top
char-3.diff
```

The updated file:

```bash
$ cat char.c
#include $<$stdio.h$>$
#include $<$string.h$>$
int main (void)
{
char ch; /* character */
return 0;
}
```

## grep

The “grep” command is used to search for patterns in the source files
(skips patch files, quilt meta-info):

```bash
$ quilt grep int
char.c: int main (void)
float.c: int main (void)
int.c:int main (void)
int.c: int x; /*integer */
```

## delete

We are now going to “delete” char-3.diff.

```bash
$ quilt pop float-2
Removing patch char-3.diff
Restoring char.c

Now at patch float-2.diff

$ cat char.c
#include $<$stdio.h$>$
int main (void)
{
char ch;
return 0;
}

$ quilt top
float-2.diff

$ quilt series
int-1.diff
float-2.diff
char-3.diff
```

The “delete” command deletes the patch from the series file:

```bash
$ quilt delete char-3

$ cat char.c
#include $<$stdio.h$>$
int main (void)
{
char ch;
return 0;
}

$ quilt top
float-2.diff

$ quilt series
int-1.diff
float-2.diff
```

The patch still exists in the patches directory though:

```bash
$ ls patches/
char-3.diff float-2.diff int-1.diff series
```

## fork

The “fork” command is used to make a copy of the topmost patch, so it
can be used for branching options:

```bash
$ quilt fork
Fork of patch float-2.diff created as float-3.diff
```

Both the old and newly forked patch exists in the patches/ directory:

```bash
$ ls patches/
char-3.diff float-2.diff float-3.diff int-1.diff series

$ quilt top
float-3.diff

$ quilt series
int-1.diff
float-3.diff

$ quilt edit float.c
```

(Add a comment “/* floating */” to the line “float f;”)

```bash
File float.c is already in patch float-3.diff

$ quilt refresh
Refreshed patch float-3.diff

$ cat patches/float-3.diff
Index: quilt-eg/float.c
=====================================
--- quilt-eg.orig/float.c 2006-04-05 22:01:01.000000000 +0530
+++ quilt-eg/float.c 2006-04-05 22:54:39.000000000 +0530
@@ -1,6 +1,7 @@
#include $<$stdio.h$>$
+#include $<$math.h$>$
int main (void)
{
- float f;
+ float f; /* floating */
return 0;
}
```

## remove

The “remove” command is used to remove the associativity of files to a
patch:

```bash
$ quilt remove -p float-3 float.c
File float.c removed from patch float-3.diff

$ quilt refresh
Nothing in patch float-3.diff

$ cat float.c
#include $<$stdio.h$>$
int main (void)
{
char ch;
return 0;
}
```

## Cleaning up

Cleaning up:

```bash
$ quilt pop int-1.diff -f
Removing patch float-3.diff
Now at patch int-1.diff

$ cat float.c
#include $<$stdio.h$>$
int main (void)
{
float f;
return 0;
}
```

```bash
$ quilt top
int-1.diff

$ quilt series
int-1.diff
float-3.diff

$ quilt delete float-3

$ quilt series
int-1.diff
```

```bash
$ quilt remove int.c
File int.c removed from patch int-1.diff

$ cat int.c
#include $<$stdio.h$>$
int main (void)
{
int x;
return 0;
}

$ quilt refresh
Nothing in patch int-1.diff

$ quilt pop -f
Removing patch int-1.diff
No patches applied

$ quilt delete int-1
```

# Links

Andreas Grünbacher et al.: Patchwork Quilt,
<http://savannah.nongnu.org/projects/quilt>.
