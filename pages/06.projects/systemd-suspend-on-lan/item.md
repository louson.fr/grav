---
title: 'Systemd suspend on lan'
feed:
    limit: 10
---

I needed an easy way to shutdown or hibernate my machine over the network. This is a method[^1] that use a systemd socket service.

[^1]:[Gitlab page](https://gitlab.com/Louson/systemd-suspend-on-lan)

Considering systemd services for shutdown and hibernation are respectively `systemd-halt.service` and `systemd-suspend.service`, we create for each a systemd socket unit file[^2]:

[^2]:`man systemd.socket`

Here is the content of `systemd-halt.socket` (`systemd-suspend.socket` is identical except the port which is 2113):
```
[Socket]
ListenStream=2112
TriggerLimitBurst=1
TriggerLimitIntervalSec=10min

[Install]
WantedBy=sockets.target
```

* `ListenStream` is the port to listen.
* `TriggerLimitBurst` and `TriggerLimitIntervalSec` specify that the socket may be activated only once every ten minutes.

This should be installed in `/usr/local/lib/systemd/system` and enabled :
```bash
$ systemctl enable systemd-halt.socket
```

This way, any connection on the port 2112 and 2113 will respectively halt and suspend the target. This can be done with a navigator. 