#include <stdlib.h>
#include <stdio.h>

#include <unistd.h>

#include <fcgiapp.h>
#include <fcgios.h>

const char * socket_path = ":2002";

static void PrintEnv(FCGX_Stream *out, char *label, char **envp)
{
    FCGX_FPrintF(out, "%s:<br>\n<pre>\n", label);
    for( ; *envp != NULL; envp++) {
        FCGX_FPrintF(out, "%s\n", *envp);
    }
    FCGX_FPrintF(out, "</pre><p>\n");
}

int main(void)
{
    FCGX_Request request = {0};
    int count = 0;

    int err = FCGX_Init();
    if (err != 0) {
        fprintf(stderr, "Failed to initialize library\n");
        return -1;
    }

    /* FCGX_Init creates a socket on standard input that we don't
       need. This call will close it. */
    FCGX_Finish();

    int socket = FCGX_OpenSocket(socket_path, 1);
    if (socket == -1) {
        fprintf(stderr, "Failed to open socket %s\n", socket_path);
        return -1;
    }

    err = FCGX_InitRequest(&request, socket, 0);
    if (err != 0) {
        fprintf(stderr, "Failed to initialize request\n");
        OS_IpcClose(request.ipcFd, -1);
        return -1;
    }

    fprintf(stderr, "Waiting for request\n");

    while (FCGX_Accept_r(&request) >= 0) {
        fprintf(stderr, "Received request\n");

        char *contentLength = FCGX_GetParam("CONTENT_LENGTH", request.envp);
        int len = 0;

        FCGX_FPrintF(request.out,
                     "Content-type: text/html\r\n"
                     "\r\n"
                     "<title>FastCGI echo (direct access version)</title>"
                     "<h1>FastCGI echo (direct access version)</h1>\n"
                     "Request number %d,  Process ID: %d<p>\n", ++count, getpid());

        if (contentLength != NULL)
            len = strtol(contentLength, NULL, 10);

        if (len <= 0) {
            FCGX_FPrintF(request.out, "No data from input.<p>\n");
        }
        else {
            int i, ch;

            FCGX_FPrintF(request.out, "Standard input:<br>\n<pre>\n");
            for (i = 0; i < len; i++) {
                if ((ch = FCGX_GetChar(request.in)) < 0) {
                    FCGX_FPrintF(request.out,
                                 "Error: Not enough bytes received on standard input<p>\n");
                    break;
                }
                FCGX_PutChar(ch, request.out);
            }
            FCGX_FPrintF(request.out, "\n</pre><p>\n");
        }

        PrintEnv(request.out, "Request environment", request.envp);
    }

    FCGX_Free(&request, 0);
    OS_LibShutdown();

    return 0;
}
