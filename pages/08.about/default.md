---
title: About
---

## About me

I am a Linux embedded developer

I share some code on [Gitlab](https://gitlab.com/Louson)

I am an Archlinux user, look at [my archwiki page](https://wiki.archlinux.org/title/User:Louson)

## Stunning books

### 2021

* **Comic** On a sunbeam by Tillie Walden (Dans un rayon de soleil)

### 2020

* **Book** The Inheritance trilogy by N. K. Jemisin (Chroniques de la Terre fracturée)
* **Book** The Earthsea cycle by Ursula Le Guin (Les contes de Terremer)

### 2018

* **Manga** Pluto by Naoki Urasawa

### 2017

* **Book** *FR* La horde du contrevent by Alain Damasio (The WindWalkers)

### 2016

* **Comic** *FR*  Goupil ou Face by Lou Lubie
* **Comic** *FR* Le mystère du monde quantique by Mathieu Buriat and Thibaut Damour

### Before

* **Comic** *FR* Un Océan d'amour by Wilfrid Lupano and Grégory Panaccione

### Some time ago

* **Book** Spin by Robert Charles Winston

### A long time ago

* **Comic** Mafalda by Quino
* **Manga** One Piece by Eichiro Oda
* **Manga** Dragon Ball by Akira Toriyama
* **Manga** Slam Dunk by Takehiko Inoue

